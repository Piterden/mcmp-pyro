<?php

return [
    'name'    => [
        'name' => 'Имя',
    ],
    'email'   => [
        'name' => 'Email',
    ],
    'phone'   => [
        'name' => 'Телефон',
    ],
    'age'     => [
        'name' => 'Возраст',
    ],
    'message' => [
        'name' => 'Сообщение',
    ],
    'created_at_datetime' => [
        'name' => 'Время создания',
    ],

];
