<?php

return [
    'title'       => 'Mcmp',
    'name'        => 'Mcmp Module',
    'description' => 'Модуль учета заявок и вопросов от пользователей',
    'section'     => [
        'questions' => 'Вопросы',
        'requests'  => 'Заявки',
    ],
];
