<?php namespace Defr\McmpModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Mcmp\McmpQuestionsEntryModel;
use Anomaly\Streams\Platform\Model\Mcmp\McmpRequestsEntryModel;
use Defr\McmpModule\Question\Contract\QuestionRepositoryInterface;
use Defr\McmpModule\Question\QuestionModel;
use Defr\McmpModule\Question\QuestionRepository;
use Defr\McmpModule\Request\Contract\RequestRepositoryInterface;
use Defr\McmpModule\Request\RequestModel;
use Defr\McmpModule\Request\RequestRepository;

/**
 * Class McmpThemeServiceProvider
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class McmpModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Addon bindings
     *
     * @var array
     */
    protected $bindings = [
        McmpRequestsEntryModel::class  => RequestModel::class,
        McmpQuestionsEntryModel::class => QuestionModel::class,
    ];

    /**
     * Addon singletons
     *
     * @var array
     */
    protected $singletons = [
        RequestRepositoryInterface::class  => RequestRepository::class,
        QuestionRepositoryInterface::class => QuestionRepository::class,
    ];

    /**
     * Addon routes
     *
     * @var array
     */
    protected $routes = [
        'request'                        => 'Defr\McmpModule\Http\Controller\FormsController@request',
        'question'                       => 'Defr\McmpModule\Http\Controller\FormsController@question',
        'admin/mcmp'                     => 'Defr\McmpModule\Http\Controller\Admin\RequestsController@index',
        'admin/mcmp/create'              => 'Defr\McmpModule\Http\Controller\Admin\RequestsController@create',
        'admin/mcmp/edit/{id}'           => 'Defr\McmpModule\Http\Controller\Admin\RequestsController@edit',
        'admin/mcmp/questions'           => 'Defr\McmpModule\Http\Controller\Admin\QuestionsController@index',
        'admin/mcmp/questions/create'    => 'Defr\McmpModule\Http\Controller\Admin\QuestionsController@create',
        'admin/mcmp/questions/edit/{id}' => 'Defr\McmpModule\Http\Controller\Admin\QuestionsController@edit',
    ];
}
