<?php namespace Defr\McmpModule\Question;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\McmpModule\Command\SendMailToManagers;

class QuestionObserver extends EntryObserver
{

    /**
     * Run after a record is created.
     *
     * @param EntryInterface $entry
     */
    public function created(EntryInterface $entry)
    {
        $this->commands->dispatch(new SendMailToManagers($entry));

        parent::created($entry);
    }
}
