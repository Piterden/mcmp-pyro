<?php namespace Defr\McmpModule\Question\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface QuestionRepositoryInterface extends EntryRepositoryInterface
{

}
