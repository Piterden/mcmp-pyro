<?php namespace Defr\McmpModule\Command;

use Anomaly\UsersModule\Role\Contract\RoleRepositoryInterface;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Defr\McmpModule\Question\Notification\NewQuestion;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class SendMailToManagers
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SendMailToManagers
{

    /**
     * Created entry
     *
     * @var EntryInterface
     */
    protected $entry;

    /**
     * Create new SendMailToManagers instance
     *
     * @param EntryInterface $entry The entry
     */
    public function __construct(EntryInterface $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Handle the command
     *
     * @param UserRepositoryInterface $users The users
     * @param RoleRepositoryInterface $roles The roles
     */
    public function handle(
        UserRepositoryInterface $users,
        RoleRepositoryInterface $roles
    )
    {
        /* @var RoleInterface $user */
        if ($role = $roles->findBySlug('question_notifiable'))
        {
            $role->getUsers()->each(
                /* @var UserInterface $user */
                function ($user)
                {
                    $user->notify(new NewQuestion($this->entry));
                }
            );
        }
    }
}
