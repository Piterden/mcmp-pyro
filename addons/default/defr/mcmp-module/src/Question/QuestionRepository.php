<?php namespace Defr\McmpModule\Question;

use Defr\McmpModule\Question\Contract\QuestionRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class QuestionRepository extends EntryRepository implements QuestionRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var QuestionModel
     */
    protected $model;

    /**
     * Create a new QuestionRepository instance.
     *
     * @param QuestionModel $model
     */
    public function __construct(QuestionModel $model)
    {
        $this->model = $model;
    }
}
