<?php namespace Defr\McmpModule\Question;

use Defr\McmpModule\Question\Contract\QuestionInterface;
use Anomaly\Streams\Platform\Model\Mcmp\McmpQuestionsEntryModel;

class QuestionModel extends McmpQuestionsEntryModel implements QuestionInterface
{

    /**
     * Gets the identifier.
     *
     * @return int The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the email.
     *
     * @return string The email.
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Gets the phone.
     *
     * @return string The phone.
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Gets the message.
     *
     * @return string The message.
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Creating time
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
}
