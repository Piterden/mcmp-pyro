<?php namespace Defr\McmpModule\Question\Notification;

use Anomaly\McmpModule\Question\Contract\QuestionInterface;
use Anomaly\Streams\Platform\Notification\Message\MailMessage;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

/**
 * Class NewData
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class NewQuestion extends Notification
{

    use Queueable;

    /**
     * User question entry.
     *
     * @var QuestionInterface
     */
    public $entry;

    /**
     * Create a new NewQuestion instance.
     *
     * @param QuestionInterface $entry
     */
    public function __construct(QuestionInterface $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  UserInterface $notifiable
     * @return array
     */
    public function via(UserInterface $notifiable)
    {
        return ['mail'];
    }

    /**
     * Return the mail message.
     *
     * @param  UserInterface $notifiable
     * @return MailMessage
     */
    public function toMail(UserInterface $notifiable)
    {
        $user  = $notifiable->toArray();
        $entry = $this->entry->toArray();

        return (new MailMessage())
            ->view('defr.module.mcmp::notifications.email')
            ->subject(trans('defr.module.mcmp::notification.question.new_subject', $user))
            ->line(trans('defr.module.mcmp::notification.question.new_line', $user));
    }
}
