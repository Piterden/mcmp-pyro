<?php namespace Defr\McmpModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

/**
 * Class McmpThemeServiceProvider
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class McmpModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-medkit';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'requests',
        'questions',
    ];
}
