<?php namespace Defr\McmpModule\Request\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class RequestFormBuilder extends FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'name' => [
            'disabled' => true,
        ],
        'phone' => [
            'disabled' => true,
        ],
        'age' => [
            'disabled' => true,
        ],
    ];
}
