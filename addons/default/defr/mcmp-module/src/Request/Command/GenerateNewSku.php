<?php namespace Defr\McmpModule\Request\Command;

use Defr\McmpModule\Request\Contract\RequestInterface;
use Defr\McmpModule\Request\Contract\RequestRepositoryInterface;

/**
 * Class McmpThemeServiceProvider
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class GenerateNewSku
{

    protected $request;

    public function __construct(RequestInterface $request)
    {
        $this->request = $request;
    }

    public function handle(RequestRepositoryInterface $requests)
    {
        $lastSku = $requests->getLastSku();


    }
}
