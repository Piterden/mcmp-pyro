<?php namespace Defr\McmpModule\Request;

use Anomaly\Streams\Platform\Model\Mcmp\McmpRequestsEntryModel;
use Defr\McmpModule\Request\Contract\RequestInterface;

class RequestModel extends McmpRequestsEntryModel implements RequestInterface
{

    /**
     * Gets the identifier.
     *
     * @return int The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the phone.
     *
     * @return string The phone.
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Gets the age.
     *
     * @return string The email.
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Creating time
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Gets the sku.
     *
     * @return string The sku.
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Gets the start sku.
     *
     * @return     string  The start sku.
     */
    public function getStartSku()
    {
        return 'A000';
    }
}
