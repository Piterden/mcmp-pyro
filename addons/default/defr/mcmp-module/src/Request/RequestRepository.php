<?php namespace Defr\McmpModule\Request;

use Defr\McmpModule\Request\Contract\RequestRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class RequestRepository extends EntryRepository implements RequestRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var RequestModel
     */
    protected $model;

    /**
     * Create a new RequestRepository instance.
     *
     * @param RequestModel $model
     */
    public function __construct(RequestModel $model)
    {
        $this->model = $model;
    }

    public function getLastSku()
    {
        if ($last = $this->model->orderBy('created_at', 'DESC')->first())
        {
            return $last->getSku();
        }

        return $this->model->getStartSku();
    }
}
