<?php namespace Defr\McmpModule\Request\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface RequestRepositoryInterface extends EntryRepositoryInterface
{

}
