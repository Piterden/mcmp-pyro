<?php namespace Defr\McmpModule\Request\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface RequestInterface
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
interface RequestInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return int The identifier.
     */
    public function getId();

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName();

    /**
     * Gets the phone.
     *
     * @return string The phone.
     */
    public function getPhone();

    /**
     * Gets the age.
     *
     * @return string The email.
     */
    public function getAge();

    /**
     * Creating time
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Gets the sku.
     *
     * @return string The sku.
     */
    public function getSku();
}
