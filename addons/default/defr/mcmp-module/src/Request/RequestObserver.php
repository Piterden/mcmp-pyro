<?php namespace Defr\McmpModule\Request;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\McmpModule\Request\Command\SendMailToManagers;
use Defr\McmpModule\Request\Command\GenerateNewSku;

class RequestObserver extends EntryObserver
{

    /**
     * Run before a record is created.
     *
     * @param EntryInterface $entry
     */
    // public function creating(EntryInterface $entry)
    // {
    //     $this->commands->dispatch(new GenerateNewSku($entry));

    //     parent::creating($entry);
    // }

    /**
     * Run after a record is created.
     *
     * @param EntryInterface $entry
     */
    public function created(EntryInterface $entry)
    {
        $this->commands->dispatch(new SendMailToManagers($entry));

        parent::created($entry);
    }
}
