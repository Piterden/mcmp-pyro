<?php namespace Defr\McmpModule\Request\Notification;

use Anomaly\Streams\Platform\Notification\Message\MailMessage;
use Anomaly\UsersModule\User\Contract\UserInterface;
use Defr\McmpModule\Request\Contract\RequestInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

/**
 * Class NewRequest
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class NewRequest extends Notification
{

    use Queueable;

    /**
     * Request entry
     *
     * @var RequestInterface
     */
    public $entry;

    /**
     * Create a new NewRequest instance.
     *
     * @param RequestInterface $entry
     */
    public function __construct(RequestInterface $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  UserInterface $notifiable
     * @return array
     */
    public function via(UserInterface $notifiable)
    {
        return ['mail'];
    }

    /**
     * Return the mail message.
     *
     * @param  UserInterface $notifiable
     * @return MailMessage
     */
    public function toMail(UserInterface $notifiable)
    {
        return (new MailMessage())
            ->view('defr.module.mcmp::notifications.email', [
                'req' => $this->entry->toArray(),
            ])
            ->subject(trans('defr.module.mcmp::notification.req_mail.subject'));
    }
}
