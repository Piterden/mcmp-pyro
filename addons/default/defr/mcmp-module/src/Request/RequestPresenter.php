<?php namespace Defr\McmpModule\Request;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class RequestPresenter extends EntryPresenter
{

    /**
     * Gets the readable age.
     *
     * @return string The age.
     */
    public function getAge()
    {
        return $this->object->getAge() == 'adult' ? 'Взрослый' : 'Ребенок';
    }

    /**
     * Gets the readable age.
     *
     * @return string The age.
     */
    public function phoneLink()
    {
        $phone = $this->object->getPhone();

        return "<a href='tel:{$phone}'>{$phone}</a>";
    }
}
