<?php namespace Defr\McmpModule\Request\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class RequestTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array|string
     */
    protected $views = [];

    /**
     * The table filters.
     *
     * @var array|string
     */
    protected $filters = [
        'search' => [
            'fields' => [
                'name',
                'age',
            ],
        ],
    ];

    /**
     * The table columns.
     *
     * @var array|string
     */
    protected $columns = [
        'name',
        'phone' => [
            'value' => 'entry.phone_link',
        ],
        'age'   => [
            'value' => 'entry.get_age',
        ],
        'entry.created_at_datetime',
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit' => [
            'type' => 'info',
            'text' => 'Просмотр',
        ],
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
        'order_by' => [
            'created_at' => 'DESC',
        ],
    ];
}
