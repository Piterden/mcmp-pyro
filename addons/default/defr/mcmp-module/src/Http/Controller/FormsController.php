<?php namespace Defr\McmpModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Defr\McmpModule\Question\Contract\QuestionRepositoryInterface;
use Defr\McmpModule\Request\Contract\RequestRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class FormsController extends PublicController
{

    /**
     * Handle request from user form
     *
     * @param  RequestRepositoryInterface $requests The requests
     * @return mixed
     */
    public function request(RequestRepositoryInterface $requests)
    {
        if (!$this->request->ajax() || $this->request->method() != 'POST')
        {
            return $this->redirect->to('/');
        }

        $data = $this->request->all();

        $validator = Validator::make($data, [
            'name'  => 'required',
            'phone' => 'required',
            'age'   => 'required',
        ]);

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        /* @var RequestInterface $request */
        if ($request = $requests->create($data))
        {
            return $this->goodResponse($request);
        }

        return $this->badResponse('Can\'t create request!');
    }

    /**
     * Handle question from user form
     *
     * @param  QuestionRepositoryInterface $questions The questions
     * @return Response
     */
    public function question(QuestionRepositoryInterface $questions)
    {
        if (!$this->request->ajax() || $this->request->method() != 'POST')
        {
            return $this->redirect->to('/');
        }

        $data = $this->request->all();

        $validator = Validator::make($data, [
            'name'    => 'required',
            'message' => 'required',
        ]);

        if (!array_get($data, 'email') && !array_get($data, 'phone'))
        {
            return $this->badResponse('Need email or phone.');
        }

        if ($validator->fails())
        {
            return $this->badResponse($validator->errors());
        }

        /* @var QuestionInterface $question */
        if ($question = $questions->create($data))
        {
            return $this->goodResponse($question);
        }

        return $this->badResponse('Can\'t create question!');
    }

    /**
     * Returns error response
     *
     * @param  string|array|null $error The error
     * @return Response
     */
    private function badResponse($error)
    {
        return $this->response->json([
            'success' => false,
            'error'   => $error,
            'request' => $this->request->all(),
        ], 400);
    }

    /**
     * Returns success response
     *
     * @param  mixed      $entry The entry
     * @return Response
     */
    private function goodResponse($entry)
    {
        return $this->response->json([
            'success' => true,
            'data'    => $entry,
            'request' => $this->request->all(),
        ], 200);
    }
}
