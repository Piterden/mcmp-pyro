<?php namespace Defr\McmpModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\McmpModule\Question\Form\QuestionFormBuilder;
use Defr\McmpModule\Question\Table\QuestionTableBuilder;

class QuestionsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  QuestionTableBuilder $table
     * @return Response
     */
    public function index(QuestionTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  QuestionFormBuilder $form
     * @return Response
     */
    public function create(QuestionFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  QuestionFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(QuestionFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
