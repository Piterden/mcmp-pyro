<?php namespace Defr\McmpModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\McmpModule\Request\Form\RequestFormBuilder;
use Defr\McmpModule\Request\Table\RequestTableBuilder;

class RequestsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  RequestTableBuilder $table
     * @return Response
     */
    public function index(RequestTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  RequestFormBuilder $form
     * @return Response
     */
    public function create(RequestFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  RequestFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(RequestFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
