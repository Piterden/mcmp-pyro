<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleMcmpCreateMcmpFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'sku'     => 'anomaly.field_type.text',
        'name'    => 'anomaly.field_type.text',
        'email'   => 'anomaly.field_type.text',
        'phone'   => 'anomaly.field_type.text',
        'message' => 'anomaly.field_type.textarea',
        'age'     => [
            'type'   => 'anomaly.field_type.select',
            'config' => [
                'options' => [
                    'adult' => 'Взрослый',
                    'child' => 'Ребенок',
                ],
            ],
        ],
    ];

}
