<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleMcmpCreateQuestionsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'questions',
        'title_column' => 'name',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'    => ['required' => true],
        'message' => ['required' => true],
        'email',
        'phone',
    ];

}
