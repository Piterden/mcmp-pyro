<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleMcmpCreateRequestsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'requests',
        'title_column' => 'name',
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'  => ['required' => true],
        'phone' => ['required' => true],
        'age'   => ['required' => true],
        'sku',
    ];

}
