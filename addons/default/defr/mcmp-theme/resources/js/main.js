import 'owl.carousel/dist/assets/owl.carousel.css'
import 'owl.carousel/dist/assets/owl.theme.default.css'

import 'magnific-popup/dist/magnific-popup.css'

require('font-awesome-webpack')

// window.$ = window.jQuery = require('jquery')

import 'bootstrap-sass'
import 'magnific-popup'
import 'owl.carousel'
// import 'jquery-form-validator'

import init from './theme/init'
import initMap from './theme/initMap'

$(document).ready(init)
