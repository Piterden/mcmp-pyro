import './styles'
import './vendor'

import init from './theme/init'
import initMap from './theme/initMap'

$(document).ready(init)
