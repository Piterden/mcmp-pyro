export default function () {
    let $slider = $(document.getElementById('slider'))

    if ($slider.length) {
        $slider.owlCarousel({
            nav: true,
            navText: [
                '<i class="fa fa-angle-left"></i>',
                '<i class="fa fa-angle-right"></i>'
            ],
            navSpeed: false,
            navElement: 'div',
            navContainer: '.owl-buttons',
            dots: false,
            smartSpeed: 500,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            autoplaySpeed: false,
            items: 1,
            itemsDesktop: false,
            itemsDesktopSmall: false,
            itemsTablet: false,
            itemsMobile: false,
            loop: true,
            center: true,
            rewind: true,
            responsive: {},
            responsiveRefreshRate: 200,
            responsiveBaseElement: window,
            fallbackEasing: 'ease-out',
            info: false,
            stageClass: 'owl-wrapper',
            stageOuterClass: 'owl-wrapper-outer',
        })
    }
}
