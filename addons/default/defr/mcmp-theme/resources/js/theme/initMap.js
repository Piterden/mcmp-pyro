/* global ymaps */

export default function () {

    let map, mcmpClinic

    if ($('#map').length) {

        ymaps.ready(function () {
            map = new ymaps.Map('map', {
                center: [59.99835076, 30.34748850],
                zoom: 14
            })

            if (map) {
                map.geoObjects.add(new ymaps.Placemark(
                    [59.99835076, 30.34748850],
                    {
                        balloonContentHeader: '<h4>Международный центр<br>медицинской профилактики.</h4>',
                        balloonContentBody: '\
<div class="address block">\
    <i class="fa fa-map-marker"></i>\
    <div class="text">\
        Адрес:<br>\
        г. Санкт-Петербург,<br>\
        пр.Пархоменко, д.29\
    </div>\
    <div class="clearfix"></div>\
</div>',
                        balloonContentFooter: '@ ' + (new Date()).getFullYear(),
                    }, {
                        preset: 'islands#blueMedicalIcon',
                        balloonCloseButton: false,
                        hideIconOnBalloonOpen: false
                    }
                ))
            }
        })
    }
}
