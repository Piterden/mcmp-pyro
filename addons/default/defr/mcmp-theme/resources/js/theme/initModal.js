import axios from 'axios'
import Modal from 'modal-vanilla'
import textMask from 'vanilla-text-mask'
import Validator from './Validator'

export default () => {
    let
        modal = new Modal({
            el: document.getElementById('static-modal'),
            backdrop: false,
            appendTo: '.page-container',
        }),

        openModal = (e) => {
            modal.show()
            modal.on('hide', () => {
                document.querySelectorAll('.modal-backdrop').forEach((el) => {
                    el.remove()
                })
            })
        },

        submitForm = (e) => {
            let
                formSelector = '.zapis-na-priem',
                dataArray = $(formSelector).serializeArray(),
                data = {},
                $modalBody = $(modal.el.querySelector('.modal-body')),
                $modalButtons = $(modal.el.querySelector('.buttons'))

            dataArray.forEach(field => {
                data[field.name] = field.value
            })

            if (data.name && data.phone) {
                $modalBody.empty().append($('\
<div class="notify-wrapper text-center">\
    <h2>Пожалуйста подождите!</h2>\
    <h5>Выполняется заявка.</h5>\
</div>\
                '))

                $modalButtons.empty()

                axios.post('/request', data, {
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                })
                .then(response => {
                    $modalBody.empty().append($('\
<div class="notify-wrapper text-center">\
    <h2>Ваша заявка принята!</h2>\
    <h5>В ближайшее время с вами свяжется наш менеджер.</h5>\
</div>\
                    '))

                    $modalButtons.append($('\
<button type="button" class="btn btn-success" data-dismiss="modal">Закрыть</button>\
                    '))

                    ga('send', 'event', 'Knopka', 'zapisatsya')

                    yaCounter43144954.reachGoal('zapisatsya')

                    setTimeout(() => {
                        modal.hide()
                    }, 15000)
                })
                .catch(error => {
                    $modalBody.empty().append($('\
<div class="notify-wrapper text-center">\
    <h2>Ой, ошибка!</h2>\
    <h6>Извините, во время выполнения заявки, произошла ошибка.</h6>\
    <h5>Попробуйте перезагрузить страницу и записаться еще раз, либо позвоните \
    <a href="tel:+78122971111">+7 (812) 297-11-11</a> и вас обязательно запишут.</h5>\
</div>\
                    '))

                    $modalButtons.append($('\
<button type="button" class="btn btn-success" data-dismiss="modal">Закрыть</button>\
<button type="button" class="btn btn-warning" onclick="(function(){window.location.reload;})();">Перезагрузить страницу</button>\
                    '))
                })
            }
        },

        phoneInput = document.getElementById('phone-field'),
        nameInput = document.getElementById('name-field'),
        openers = document.querySelectorAll('[data-modal-open]'),
        form = document.getElementById('submit-form'),

        vally = new Validator(),

        validate = (e) => {
            vally.check(e)
        },

        initForm = () => {
            textMask({
                inputElement: phoneInput,
                mask: ['+', '7', ' ', '(', /[1-9]/, /\d/, /\d/, ')',
                    ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, '-', /\d/, /\d/],
            })

            phoneInput.removeEventListener('keyup', validate)
            phoneInput.addEventListener('keyup', validate)

            nameInput.removeEventListener('keyup', validate)
            nameInput.addEventListener('keyup', validate)
        }

    initForm()

    openers.forEach(opener => {
        opener.removeEventListener('click', openModal)
        opener.addEventListener('click', openModal)
    })

    form.removeEventListener('click', submitForm)
    form.addEventListener('click', submitForm)
}
