import axios from 'axios'

import initMap from './initMap'
import initModal from './initModal'
import initSlider from './initSlider'

let
    $ajaxContainer = $('#ajax-container'),

    referer,

    titlePrefix = (function (string, separator) {
        let re = new RegExp('^.*' + separator),
            match = string.replace('\n', ' ').match(re)
        if (match && match.length) {
            return match[0].replace(/\s{2,}/, ' ')
        }
        return ''
    })($('head title').text().trim(), '›')

/**
 * Gets the active hrefs.
 *
 * @return     {Array}   The active hrefs.
 */
export const getActiveHrefs = () => {
    let
        segments = window.location.pathname.slice(1).split('/'),
        hrefs = [],
        link = window.location.origin

    for (let i = 0; i < segments.length; i++) {
        link +=  '/' + segments[i]
        hrefs.push(link)
    }

    return hrefs
}

/**
 * Gets the active links.
 *
 * @param      {Array}  hrefs   The hrefs
 * @return     {jQuery}   The active links.
 */
export const getActiveLinks = (hrefs) => {
    let
        $activeLinks = $('[href="' + hrefs[0] + '"]'),
        hasNews = function (href) {
            return href.includes('articles/news')
        }

    if (hrefs.findIndex(hasNews) !== -1) {
        $activeLinks = $('[href="' + window.location.origin + '/news"]')
    }

    hrefs.forEach((href, index) => {
        if (index !== 0) {
            $activeLinks.add('[href="' + href + '"]')
        }
    })

    return $activeLinks
}

/**
 * Adds an active class.
 */
export const addActiveClass = () => {
    let
        hrefs = getActiveHrefs(),
        $activeLinks = getActiveLinks(hrefs)

    $('.active').removeClass('active')

    $activeLinks.each((index, link) => {
        let $link = $(link)

        $link.addClass('active')
        $link.parent('li').addClass('expanded')
        $link.parents('.side-menu-block').addClass('expanded')
    })
}

export const addResponsiveClassToTables = () => {
    $('table').wrap('<div class="table-responsive"></div>')
}

/**
 * Init page
 *
 * @param      {Function}  xhrRun  The xhr run
 */
export const initFunc = (xhrRun) => {
    initMap()
    initModal()
    initSlider()

    addActiveClass()
    addResponsiveClassToTables()

    $('.side-menu-block')
    .off('click')
    .on('click', (e) => {
        let $this = $(e.target)
        if (!$this.hasClass('expanded')) {
            $('.expanded').removeClass('expanded')
            $this.addClass('expanded')
        } else {
            $('.expanded').removeClass('expanded')
        }
    })

    $('a:not([href^="http"])')
        .add('a[href^="' + window.location.origin + '"]')
        .off('click')
        .on('click', xhrRun)

    $(window).on('popstate', xhrRun)
}

/**
 * Scroll body to the top
 *
 * @param      {Jquery}  $h1     The h1
 */
export const bodyScrollTop = ($h1) => {
    $('html,body').animate({ scrollTop: 0 }, 400)
}

/**
 * After AJAX complete
 *
 * @param      {Object}  response   The response
 * @param      {String}  eventType  The event type
 */
export const xhrCb = (response, eventType) => {
    let
        scrollTop = $(window).scrollTop(),
        offsetTop = 0,
        pagetitle,
        $h1

    $ajaxContainer.append(response.data)

    $h1 = $ajaxContainer.find('h1')
    pagetitle = $h1.text()

    bodyScrollTop($h1)

    $('head title').text(titlePrefix + ' ' + pagetitle)

    yaCounter43144954.hit(window.location, {
        title: pagetitle,
        referer: referer,
    })

    initFunc(xhrRun)
}

/**
 * AJAX load main frame function
 *
 * @param      {Event}    e         Event
 * @param      {String}   pathname  The pathname
 * @return     {Boolean}
 */
export const xhrRun = function (e, pathname) {
    let
        $this = $(e.target),
        minDelay = 150,
        startTime = new Date(),
        eventType,
        url

    e.preventDefault()

    if (e !== undefined && e) {
        eventType = e.type
    }

    if ($this.data('scroll')) {
        bodyScrollTop($('h1'))
        return false
    }

    if (!pathname) {
        pathname = window.location.pathname
        url = eventType === 'popstate' ? pathname : this.href
    } else {
        url = pathname
    }

    referer = window.location

    $ajaxContainer.empty()

    axios.get(url, {
        headers: {'X-Requested-With': 'XMLHttpRequest'},
    })
    .then((response) => {
        let
            endTime,
            time,
            $header,
            $sideBlock

        if (url !== window.location && e.type !== 'popstate'){
            window.history.pushState({ path: url }, '', url)
        }

        $('.active').removeClass('active')
        $('.expanded').removeClass('expanded')

        $header = $this.parents('.header-links')

        if ($header.length) {
            $this.addClass('active')
        }

        $sideBlock = $this.parents('.side-menu-block')

        if ($sideBlock.length) {
            $this.parent().addClass('expanded')
            $sideBlock.addClass('expanded')
        }

        addActiveClass()

        endTime = new Date()
        time = endTime - startTime

        if (time < minDelay) {
            setTimeout(() => {
                xhrCb(response, e.type)
            }, minDelay - time)
        } else {
            xhrCb(response, e.type)
        }
    })
    // .catch(function (error) {
    //     window.location.href = url
    // })

    return false
}

export default initFunc(xhrRun)
