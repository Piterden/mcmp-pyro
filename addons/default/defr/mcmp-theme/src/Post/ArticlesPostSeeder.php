<?php namespace Defr\McmpTheme\Post;

use Anomaly\PostsModule\Category\Contract\CategoryRepositoryInterface;
use Anomaly\PostsModule\Post\Contract\PostRepositoryInterface;
use Anomaly\PostsModule\Type\Contract\TypeRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\Streams\Platform\Model\Posts\PostsArticlePostsEntryModel;

/**
 * Class ArticlesPostSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class ArticlesPostSeeder extends Seeder
{

    /**
     * The post repository.
     *
     * @var PostRepositoryInterface
     */
    protected $posts;

    /**
     * The type repository.
     *
     * @var TypeRepositoryInterface
     */
    protected $types;

    /**
     * The category repository.
     *
     * @var CategoryRepositoryInterface
     */
    protected $categories;

    /**
     * Create a new ArticlesPostSeeder instance.
     *
     * @param PostRepositoryInterface     $posts
     * @param TypeRepositoryInterface     $types
     * @param CategoryRepositoryInterface $categories
     */
    public function __construct(
        PostRepositoryInterface $posts,
        TypeRepositoryInterface $types,
        CategoryRepositoryInterface $categories
    )
    {
        $this->posts      = $posts;
        $this->types      = $types;
        $this->categories = $categories;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting articles seeder!\n";

        if (!$posts = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/site_content.json'),
            true
        ))
        {
            throw new \Exception('JSON error in `site_content.json` file');
        }

        if (!$tvs = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/tmpl_vars.json'),
            true
        ))
        {
            throw new \Exception('JSON error in `tmpl_vars.json` file');
        }

        $categories = array_where($posts, function ($post)
        {
            return array_get($post, 'parent') == 67;
        });

        $type = $this->types->findBySlug('article');

        foreach ($categories as $cat)
        {
            $cat_id   = array_get($cat, 'id');
            $cat_slug = array_get($cat, 'alias');

            $category = $this->categories->findBySlug($cat_slug);

            $articles = array_where($posts, function ($post) use ($cat_id)
            {
                return array_get($post, 'parent') == $cat_id;
            });

            foreach ($articles as $post)
            {
                $id         = array_get($post, 'id');
                $slug       = array_get($post, 'alias');
                $title      = array_get($post, 'pagetitle');
                $summary    = array_get($post, 'introtext');
                $content    = array_get($post, 'content');
                $enabled    = array_get($post, 'published');
                $publish_at = array_get($post, 'publishedon');

                if (array_get($post, 'uri_override'))
                {
                    $slug = array_get(
                        array_reverse(
                            explode(
                                '/',
                                rtrim(array_get($post, 'uri'), '/')
                            )
                        ),
                        0
                    );
                }

                $i = 0;
                while ($this->posts->findBySlug($slug))
                {
                    $slug = $slug . '-' . $i;
                    ++$i;
                }

                $post_tvs = array_where($tvs, function ($tv) use ($id)
                {
                    return array_get($tv, 'contentid') == $id;
                });

                $meta_title = array_get(array_first($post_tvs, function ($tv)
                {
                    return array_get($tv, 'tmplvarid') == 19;
                }), 'value');

                $meta_description = array_get(array_first($post_tvs, function ($tv)
                {
                    return array_get($tv, 'tmplvarid') == 17;
                }), 'value');

                $entry = (new PostsArticlePostsEntryModel())->create([
                    'ru' => [
                        'content' => $content,
                    ],
                ]);

                $this->posts->create([
                    'ru'         => [
                        'title'            => $title,
                        'summary'          => $summary,
                        'meta_title'       => $meta_title,
                        'meta_description' => $meta_description,
                    ],
                    'slug'       => $slug,
                    'created_at' => $publish_at,
                    'publish_at' => $publish_at,
                    'enabled'    => $enabled,
                    'type'       => $type,
                    'entry'      => $entry,
                    'category'   => $category,
                    'author_id'  => 1,
                ]);

                echo "\033[36;5;228mCreated article \033[31;5;228m{$title}\n";
            }
        }

        echo "\033[32;5;228mArticles was seeded successfully!\n";
    }
}
