<?php namespace Defr\McmpTheme\Post;

use Anomaly\PostsModule\Category\Contract\CategoryRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;

/**
 * Class CategoriesPostSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class CategoriesPostSeeder extends Seeder
{

    /**
     * The category repository.
     *
     * @var CategoryRepositoryInterface
     */
    protected $categories;

    /**
     * Create a new CategoriesPostSeeder instance.
     *
     * @param CategoryRepositoryInterface $categories
     */
    public function __construct(
        CategoryRepositoryInterface $categories
    )
    {
        $this->categories = $categories;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting posts categories seeder!\n";

        if (!$data = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/site_content.json'),
            true
        ))
        {
            throw new \Exception('JSON error in `site_content.json` file');
        }

        $this->categories->truncate();

        echo "\033[35;5;228mCategories cleared!\n";

        $categories = array_where($data, function ($post)
        {
            return array_get($post, 'parent') == 67;
        });

        $this->categories->create([
            'ru'   => [
                'name'        => 'Новости',
                'description' => 'Новости и обновления информации.',
            ],
            'slug' => 'news',
        ]);

        echo "\033[36;5;228mCreated post category \033[31;5;228mНовости\n";

        foreach ($categories as $category)
        {
            $title      = array_get($category, 'pagetitle');
            $slug       = array_get($category, 'alias');
            $summary    = array_get($category, 'introtext');
            $publish_at = array_get($category, 'publishedon');

            if (array_get($category, 'uri_override'))
            {
                $slug = array_get(
                    array_reverse(
                        explode(
                            '/',
                            rtrim(array_get($post, 'uri'), '/')
                        )
                    ),
                    0
                );
            }

            $this->categories->create([
                'ru'         => [
                    'name'        => $title,
                    'description' => $summary,
                ],
                'slug'       => $slug,
                'created_at' => $publish_at,
            ]);

            echo "\033[36;5;228mCreated post category \033[31;5;228m{$title}\n";
        }

        echo "\033[32;5;228mCategories was seeded successfully!\n";
    }
}
