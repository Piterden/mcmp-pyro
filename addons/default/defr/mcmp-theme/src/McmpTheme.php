<?php namespace Defr\McmpTheme;

use Anomaly\Streams\Platform\Addon\Theme\Theme;

/**
 * Class McmpTheme
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class McmpTheme extends Theme
{

}
