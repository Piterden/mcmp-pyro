<?php namespace Defr\McmpTheme;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

/**
 * Class McmpThemeServiceProvider
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class McmpThemeServiceProvider extends AddonServiceProvider
{

    /**
     * Templates overrides
     *
     * @var array
     */
    protected $overrides = [
        'streams::errors.404'                        => 'theme::errors.404',
        'anomaly.module.posts::posts.view'           => 'theme::posts.view',
        'anomaly.module.posts::posts.view'           => 'theme::posts.view',
        'anomaly.module.posts::posts.partials.posts' => 'theme::posts.partials.posts',
        'anomaly.module.posts::categories.index'     => 'theme::categories.index',
    ];
}
