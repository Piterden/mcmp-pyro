<?php namespace Defr\McmpTheme\Repeater;

use Anomaly\PagesModule\Page\Contract\PageRepositoryInterface;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Anomaly\Streams\Platform\Model\Pages\PagesAnalizyPagesEntryModel;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterPrajsListEntryModel;

/**
 * Class PrajsListSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class PrajsListSeeder extends Seeder
{

    /**
     * The pages repository.
     *
     * @var PageRepositoryInterface
     */
    protected $pages;

    /**
     * The prices repository.
     *
     * @var RepeaterPrajsListEntryModel
     */
    protected $price;

    /**
     * Create a new PrajsListSeeder instance.
     *
     * @param PageRepositoryInterface     $pages
     * @param RepeaterPrajsListEntryModel $pages
     */
    public function __construct(
        PageRepositoryInterface $pages,
        RepeaterPrajsListEntryModel $price
    )
    {
        $this->pages = $pages;
        $this->price = $price;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        echo "\n\033[37;5;228mStarting price list seeder!\n";

        $this->price->truncate();

        echo "\033[35;5;228mPrice lists cleared!\n";

        if (!$resources = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/site_content.json'),
            true
        ))
        {
            throw new \Exception('JSON error in `site_content.json` file');
        }

        if (!$analizy = json_decode(
            file_get_contents(__DIR__ . '/../../resources/seeder/analizy.json'),
            true
        ))
        {
            throw new \Exception('JSON error in `analizy.json` file');
        }

        $byParent = [];

        foreach ($analizy as $analiz)
        {
            $artikul             = array_get($analiz, 'sku');
            $naimenovanie_uslugi = array_get($analiz, 'name');
            $dlitel_nost         = array_get($analiz, 'time');
            $price               = array_get($analiz, 'price');

            $entry = $this->price->create([
                'artikul'             => $artikul,
                'naimenovanie_uslugi' => $naimenovanie_uslugi,
                'dlitel_nost'         => $dlitel_nost,
                'price'               => $price,
            ]);

            $id = array_get($analiz, 'resource');

            if (!array_get($byParent, $id))
            {
                array_set($byParent, $id, []);
            }

            array_push($byParent[$id], $entry->getId());
        }

        foreach ($byParent as $id => $repeater)
        {
            $slug = array_get(array_first(
                $resources,
                function ($resource) use ($id)
                {
                    return array_get($resource, 'id') == $id;
                }
            ), 'alias');

            $path = '/services/analizy/' . $slug;
            $page = $this->pages->findByPath(str_replace(',', '', $path));

            if ($page)
            {
                if ($pageEntry = $page->getEntry())
                {
                    if ($pageEntry instanceof PagesAnalizyPagesEntryModel)
                    {
                        $pageEntry->prajs_dlya_analizov = $repeater;

                        $page->save();

                        echo "\033[36;5;228mAdded price list for page \033[31;5;228m{$slug}\n";

                        continue;
                    }
                }
            }

            echo "\033[31;5;228mNot Added price list for page \033[33;5;228m{$slug}\n";
        }

        echo "\033[32;5;228mPrice lists analizy was seeded successfully!\n";
    }
}
