<?php namespace Defr\McmpTheme;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\McmpTheme\Post\ArticlesPostSeeder;
use Defr\McmpTheme\Post\CategoriesPostSeeder;
use Defr\McmpTheme\Post\NewsPostSeeder;

// use Defr\McmpTheme\Repeater\PrajsListSeeder;

/**
 * Class McmpThemeSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com >
 *
 * @link   http://pyrocms.com/
 */
class McmpThemeSeeder extends Seeder
{

    /**
     * Run the seeder.
     */
    public function run()
    {
        $this->call(CategoriesPostSeeder::class);
        $this->call(NewsPostSeeder::class);
        $this->call(ArticlesPostSeeder::class);

        // $this->call(PrajsListSeeder::class);
    }
}
