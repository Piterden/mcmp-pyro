/**
 * Init page
 *
 * @param      {Function}  xhrHandler  The xhr run
 */
export const initFunction = (xhrHandler) => {
    $('a:not([href^="http"])')
        .add('a[href^="' + window.location.origin + '"]')
        .off('click')
        .on('click', xhrHandler)

    $(window).on('popstate', xhrHandler)
}

/**
 * After AJAX complete
 *
 * @param      {Object}  response   The response
 * @param      {String}  eventType  The event type
 */
export const xhrCallback = (response, eventType) => {
    let
        pagetitle,
        $h1

    $ajaxContainer.append(response.data)

    $h1 = $ajaxContainer.find('h1')
    pagetitle = $h1.text()

    $('head title').text(titlePrefix + ' ' + pagetitle)

    yaCounterXXXXXXXX.hit(window.location, {
        title: pagetitle,
        referer: referer,
    })

    initFunction(xhrHandler)
}

/**
 * AJAX load main frame function
 *
 * @param      {Event}    e         Event
 * @param      {String}   pathname  The pathname
 * @return     {Boolean}
 */
export const xhrHandler = function (e, pathname) {
    let
        $this = $(e.target),
        eventType,
        url

    e.preventDefault()

    if (e !== undefined && e) {
        eventType = e.type
    }

    if (!pathname) {
        pathname = window.location.pathname
        url = eventType === 'popstate' ? pathname : this.href
    } else {
        url = pathname
    }

    referer = window.location

    axios.get(url, {
        headers: {'X-Requested-With': 'XMLHttpRequest'},
    })
    .then((response) => {
        if (url !== window.location && e.type !== 'popstate'){
            window.history.pushState({ path: url }, '', url)
        }

        xhrCallback(response, e.type)
    })
    // .catch(function (error) {
    //     window.location.href = url
    // })

    return false
}

export default initFunction(xhrHandler)
