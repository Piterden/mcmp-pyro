<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
 */

Route::get('/', function ()
{
    return view('welcome');
});

$posts_uri = 'articles';

Route::get($posts_uri . '/rss/categories/{category}.xml', [
    'as'   => 'anomaly.module.posts::categories.rss',
    'uses' => 'Anomaly\PostsModule\Http\Controller\RssController@category',
]);

Route::get($posts_uri . '/rss/tags/{tag}.xml', [
    'as'   => 'anomaly.module.posts::tags.rss',
    'uses' => 'Anomaly\PostsModule\Http\Controller\RssController@tag',
]);

Route::get($posts_uri . '/rss.xml', [
    'as'   => 'anomaly.module.posts::posts.rss',
    'uses' => 'Anomaly\PostsModule\Http\Controller\RssController@recent',
]);

Route::get($posts_uri, [
    'as'   => 'anomaly.module.posts::posts.index',
    'uses' => 'Anomaly\PostsModule\Http\Controller\PostsController@index',
]);

Route::get($posts_uri . '/preview/{str_id}', [
    'as'   => 'anomaly.module.posts::posts.preview',
    'uses' => 'Anomaly\PostsModule\Http\Controller\PostsController@preview',
]);

Route::get($posts_uri . '/tags/{tag}', [
    'as'   => 'anomaly.module.posts::tags.view',
    'uses' => 'Anomaly\PostsModule\Http\Controller\TagsController@index',
]);

Route::get($posts_uri . '/{slug}', [
    'as'   => 'anomaly.module.posts::categories.view',
    'uses' => 'Anomaly\PostsModule\Http\Controller\CategoriesController@index',
]);

Route::get($posts_uri . '/archive/{year}/{month?}', [
    'as'   => 'anomaly.module.posts::tags.archive',
    'uses' => 'Anomaly\PostsModule\Http\Controller\ArchiveController@index',
]);

Route::get($posts_uri . '/{category}/{slug}', [
    'as'   => 'anomaly.module.posts::posts.view',
    'uses' => 'Anomaly\PostsModule\Http\Controller\PostsController@view',
]);
